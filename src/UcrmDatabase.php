<?php /** @noinspection PhpUnused */
declare(strict_types=1);

namespace SpaethTech\UCRM\SDK;

use PDO;

class UcrmDatabase
{

    protected const DEFAULTS_PDO_OPTIONS = [
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    ];

    protected static string $databaseHost;
    protected static int $databasePort;
    protected static string $databaseName;
    protected static string $databaseUser;
    protected static string $databasePass;

    /** @var PDO|null The database object. */
    private static PDO|null $pdo = null;

    /** @var string[] */
    private static array $schemas;

    #region CONNECTION

    /**
     * Attempts a connection to the database or simply returns an existing connection unless otherwise requested.
     *
     * @param bool $reconnect If TRUE, then forces a new database (re-)connection to be made, defaults to FALSE.
     *
     * @return PDO|false Returns a valid database object for use with database commands.
     */
    public static function connect(bool $reconnect = false): PDO|false
    {
        // IF the connection already exists AND a reconnect was not requested, THEN return the current database object!
        if (self::$pdo !== null && !$reconnect)
            return self::$pdo;

        if ((self::$databaseHost = $host = \SpaethTech\UCRM\SDK\ServerParameters::get("database_host")) === null)
            return \SpaethTech\UCRM\SDK\PluginLog::error("[UcrmDatabase] Could not determine database host!", [], false);
        if ((self::$databasePort = $port = \SpaethTech\UCRM\SDK\ServerParameters::get("database_port")) === null)
            return \SpaethTech\UCRM\SDK\PluginLog::error("[UcrmDatabase] Could not determine database port!", [], false);
        if ((self::$databaseName = $name = \SpaethTech\UCRM\SDK\ServerParameters::get("database_name")) === null)
            return \SpaethTech\UCRM\SDK\PluginLog::error("[UcrmDatabase] Could not determine database name!", [], false);
        if ((self::$databaseUser = $user = \SpaethTech\UCRM\SDK\ServerParameters::get("database_user")) === null)
            return \SpaethTech\UCRM\SDK\PluginLog::error("[UcrmDatabase] Could not determine database user!", [], false);
        if ((self::$databasePass = $pass = \SpaethTech\UCRM\SDK\ServerParameters::get("database_password")) === null)
            return \SpaethTech\UCRM\SDK\PluginLog::error("[UcrmDatabase] Could not determine database pass!", [], false);

        // NOTE: All pre-checks should have ensured a valid state for connection!

        try
        {
            // Attempt to create a new database connection using the provided information.
            self::$pdo = new PDO("pgsql:host=$host;port=$port;dbname=$name", $user, $pass, self::DEFAULTS_PDO_OPTIONS);

            self::$schemas = array_map(
                function($schema)
                {
                    return $schema["schema_name"];
                },
                self::$pdo->query("SELECT schema_name FROM information_schema.schemata")->fetchAll()
            );

            // IF the connection is valid, return the new database object!
            if(self::$pdo)
                return self::$pdo;
        }
        catch(PDOException $e)
        {
            // OTHERWISE, log the error and return false!
            return \SpaethTech\UCRM\SDK\PluginLog::error("[UcrmDatabase] An error occurred while connecting!",
                [ "message" => $e->getMessage() ], false);
        }

        // We should NEVER reach this line of code, but if we somehow do, log the error and return false!
        return \SpaethTech\UCRM\SDK\PluginLog::error("[UcrmDatabase] An unknown error occurred while connecting!", [], false);
    }

    #endregion

    #region HELPERS

    private static function encodeColumns(array|string $columns): string
    {
        if ($columns === [] || $columns === "*")
            return "*";

        if (is_string($columns))
            $columns = explode(",", $columns);

        $columns = array_map("trim", $columns);

        return "\"".implode("\",\"", $columns)."\"";
    }

    private static function encodeTable(string $table): string
    {
        /** @noinspection PhpUnusedLocalVariableInspection */
        [$database, $schema, $table] = array_values(self::parseTable($table));

        //if($schema !== "")
        //    $pdo->exec("SET search_path TO $schema");

        return ($schema !== "" ? "\"$schema\"." : "") . "\"$table\"";


    }



    private static function parseTable(string $table): array|false
    {
        $results = [
            "database"  =>  "",
            "schema"    =>  "",
            "table"     =>  $table,
        ];

        if(str_contains($table, ".")) // Strings::contains($table, "."))
        {
            $_database  = "";
            $_schema    = "";
            //$_table     = "";

            switch(count($parts = explode(".", $table)))
            {
                case 1  :   [$_table]                       = $parts;   break;
                case 2  :   [$_schema, $_table]             = $parts;   break;
                case 3  :   [$_database, $_schema, $_table] = $parts;   break;

                default :   return \SpaethTech\UCRM\SDK\PluginLog::error("[UcrmDatabase] Invalid table format!",
                                [ "format" => "[[database.]schema.]table" ], false);
            }

            if($_database !== "" && $_database !== self::$databaseName)
                return \SpaethTech\UCRM\SDK\PluginLog::error("[UcrmDatabase] Database mismatch '$_database' in '$table'!", [], false);

            if($_schema !== "" && !in_array($_schema, self::$schemas))
                return \SpaethTech\UCRM\SDK\PluginLog::error("[UcrmDatabase] Schema '$_schema' not found!", [], false);

            $results = [
                "database"  =>  $_database,
                "schema"    =>  $_schema,
                "table"     =>  $_table,
            ];

        }

        return $results;
    }



    #endregion

    #region QUERYING

    public static function query(string $query, array $params = []): array
    {
        // Get a connection to the database.
        $pdo = self::connect();

        // Generate a SQL statement, given the provided parameters.
        $sql = $pdo->prepare($query);
        $sql->execute($params);

        // Execute the query and return the results!
        return $sql->fetchAll();
    }

    /**
     * Issues a SELECT query to the database.
     *
     * @param string $table The table for which to make the query.
     * @param array $columns An optional array of column names to be returned.
     * @param string $suffix
     *
     * @return array Returns an associative array of rows from the database.
     */
    public static function select(string $table, array $columns = [], string $suffix = ""): array
    {
        // Get a connection to the database.
        $pdo = self::connect();

        $columns = self::encodeColumns($columns);
        $table = self::encodeTable($table);

        $sql = <<<EOF
            SELECT $columns
            FROM $table
            $suffix;
            EOF;

        // Execute the query and return the results!
        return $pdo->query($sql)->fetchAll();
    }






    #endregion


}
