<?php /** @noinspection PhpUnused, SqlNoDataSourceInspection */

namespace SpaethTech\UCRM\SDK;

use PDO;

class PluginDatabase
{
    /**
     * @var PDO|null The Plugin's internal database handle.
     */
    protected PDO|null $pdo = null;

    public function __construct(protected string $path = PLUGIN_DIR."/data/plugin.db")
    {
    }

    /**
     * Gets the Plugin's own database path.
     *
     * @return string Returns the database path, even if it does not exist.
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * Gets the Plugin's internal database handle.
     *
     * @return PDO|null Returns the database handle, or NULL if not connected!
     */
    public function getPDO() : PDO|null
    {
        return $this->pdo;
    }



    /**
     * Connects to the Plugin's internal database.
     *
     * @return PDO|null Returns the database handle if the connection was successful, otherwise NULL!
     */
    public function connect(): PDO|null
    {
        // IF a database handle does not already exist...
        if (!$this->pdo)
        {
            try
            {
                $this->pdo = new PDO(
                    "sqlite:" . $this->path,
                    null,
                    null,
                    [
                        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
                    ]
                );
            }
            catch(PDOException) //$e)
            {
                return null;
                //http_response_code(400);
                //die("The Plugin's Database could not be opened!\n$e");
            }
        }

        // OTHERWISE, return the existing handle.
        return $this->pdo;
    }

    /**
     * Queries the Plugin's internal database.
     *
     * @param string $statement The query to execute, must be valid SQL syntax.
     *
     * @return array Returns an array of fetched results.
     */
    public function query(string $statement, array $params = [], ?string $class = null): array
    {
        try
        {
            $sql = $this->connect()->prepare( $statement );
            $sql->execute( $params );

            $results = ($class !== null)
                ? $sql->fetchAll( PDO::FETCH_CLASS, $class )
                : $sql->fetchAll();

            //$results = self::dbConnect()->query($statement)->fetchAll();
            //return $results ?: [];
            /*
            $pdo = self::dbConnect();
            $results = ($class !== null)
                ? $pdo->query($statement)->fetchAll( PDO::FETCH_CLASS, $class )
                : $pdo->query($statement)->fetchAll();
            */

            return $results ?: [];
        }
        catch(PDOException $e)
        {
            http_response_code(400);
            die("The Plugin's Database could not be accessed!\n$e");
        }
    }

    /**
     * Closes the Plugin's internal database connection.
     */
    public function close(): void
    {
        if ($this->pdo !== null)
            $this->pdo = NULL;
    }

    /**
     * Deletes the Plugin's internal database, closing the connection as needed.
     */
    public function delete(): void
    {
        $this->close();
        unlink($this->getPath());
    }

    /**
     *
     * @param string $name
     *
     * @return bool
     */
    public function tableExists(string $name): bool
    {
        return count($this->query(/** @lang SQLite */ <<<EOF
            -- noinspection SqlResolve
            SELECT *
            FROM `sqlite_master`
            WHERE `type` = 'table' AND `name` = :name;
            EOF
            , [ ":name" => $name ] ) ) > 0;
    }

    /**
     *
     * @return \SpaethTech\UCRM\SDK\Permissions\Permission[]
     */
    public function permissions(): array
    {
        if (!$this->tableExists("permissions"))
        {
            $this->query(/** @lang SQLite */ <<<EOF
                CREATE TABLE `permissions` (
                    `id`         INTEGER PRIMARY KEY AUTOINCREMENT,
                    `group_id`   TEXT,
                    `group_name` TEXT,
                    `user_id`    INTEGER,
                    `user_name`  TEXT,
                    `type`       TEXT NOT NULL,
                    `key`        TEXT NOT NULL,
                    `value`      TEXT NOT NULL,
                    `allowed`    INTEGER
                );
                EOF
            );

            $this->query(/** @lang SQLite */ <<<EOF
                -- noinspection SqlResolve
                INSERT INTO `permissions`
                VALUES (
                    null, null, 'Admin Group', null, null, :type, :key, :value, true
                )
                EOF,
                [
                    ":type" => \SpaethTech\UCRM\SDK\Permissions::ROUTE->value,
                    ":key" => "api",
                    ":value" => "/api/*",
                ]
            );

        }

        return $this->query(/** @lang SQLite */ <<<EOF
            -- noinspection SqlResolve
            SELECT *
            FROM `permissions`
            EOF,
            [],
            \SpaethTech\UCRM\SDK\Permissions::class
        );

    }

}
