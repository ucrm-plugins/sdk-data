<?php

namespace SpaethTech\UCRM\SDK\Data\Parsers;

use DateTime;

class DateTimeParser
{
    private const TIMESTAMP_FORMAT = "Y-m-d H:i:s";

    public static function setTimestamp(string $timestamp) : DateTime
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        return new DateTime($timestamp);
    }

    public static function getTimestamp(DateTime $timestamp) : string
    {
        return $timestamp->format(self::TIMESTAMP_FORMAT);
    }

    public static function setTimestampNullable(string|null $timestamp) : DateTime|null
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        return $timestamp !== null ? new DateTime($timestamp) : null;
    }

    public static function getTimestampNullable(DateTime|null $timestamp) : string|null
    {
        return $timestamp?->format(self::TIMESTAMP_FORMAT);
    }

}
