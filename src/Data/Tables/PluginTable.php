<?php
/** @noinspection PhpUnused */
declare(strict_types=1);

namespace SpaethTech\UCRM\SDK\Data\Tables;

use CollectionClass;
use Data\Attributes\DatabaseColumn;
use Data\Attributes\DatabaseTable;
use Data\Collections\PluginCollection;

/**
 * Class Plugin
 *
 * @package UCRM\UNMS
 * @author Ryan Spaeth <rspaeth@spaethtech.com>
 * @final
 */
#[DatabaseTable("ucrm.plugin")]
#[CollectionClass(PluginCollection::class)]
final class PluginTable extends AutoTable
{
    #[DatabaseColumn("id")]
    protected int $id;

    #[DatabaseColumn("name")]
    protected string $name;

    #[DatabaseColumn("display_name")]
    protected string $displayName;

    #[DatabaseColumn("description")]
    protected string $description;

    #[DatabaseColumn("url")]
    protected string|null $url;

    #[DatabaseColumn("author")]
    protected string $author;

    #[DatabaseColumn("version")]
    protected string $version;

    #[DatabaseColumn("enabled")]
    protected bool $enabled;

    #[DatabaseColumn("execution_period")]
    protected string|null $executionPeriod;

    #[DatabaseColumn("min_unms_version")]
    protected string|null $minUnmsVersion;

    #[DatabaseColumn("max_unms_version")]
    protected string|null $maxUnmsVersion;

    #[DatabaseColumn("has_widgets")]
    protected bool $hasWidgets;

    #[DatabaseColumn("has_payment_button")]
    protected bool $hasPaymentButton;

    #[DatabaseColumn("has_admin_zone_js")]
    protected bool $hasAdminZoneJS;

    #[DatabaseColumn("has_client_zone_js")]
    protected bool $hasClientZoneJS;

}
