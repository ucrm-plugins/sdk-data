<?php
/** @noinspection PhpUnused */
declare(strict_types=1);

namespace SpaethTech\UCRM\SDK\Data\Tables;

use CollectionClass;
use Data\Attributes\DatabaseColumn;
use Data\Attributes\DatabaseTable;
use Data\Collections\OptionCollection;

/**
 * Class Option
 *
 * @package MVQN\UNMS
 * @author Ryan Spaeth <rspaeth@spaethtech.com>
 * @final
 *
 * @extends AutoTable<OptionTable>
 */
#[DatabaseTable("ucrm.option")]
#[CollectionClass(OptionCollection::class)]
final class OptionTable extends AutoTable
{
    #[DatabaseColumn("option_id")]
    public int $optionId;

    #[DatabaseColumn("code")]
    public string $code;

    #[DatabaseColumn("value")]
    public string|null $value;

}
