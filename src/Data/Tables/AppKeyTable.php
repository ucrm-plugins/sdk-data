<?php
/** @noinspection PhpUnused */
declare(strict_types=1);

namespace SpaethTech\UCRM\SDK\Data\Tables;

use CollectionClass;
use Data\Attributes\DatabaseColumn;
use Data\Attributes\DatabaseTable;
use Data\Collections\AppKeyCollection;
use Data\Parsers\DateTimeParser;
use DateTime;

/**
 * Class AppKey
 *
 * @package UCRM\UNMS
 * @author Ryan Spaeth <rspaeth@spaethtech.com>
 * @final
 */
#[DatabaseTable("ucrm.app_key")]
#[CollectionClass(AppKeyCollection::class)]
final class AppKeyTable extends AutoTable
{
    #[DatabaseColumn("key_id")]
    public int $keyId;

    #[DatabaseColumn("name")]
    public string $name;

    #[DatabaseColumn("key")]
    public string|null $key;

    #[DatabaseColumn("type")]
    public string $type;

    #[DatabaseColumn("created_date", [ DateTimeParser::class, "setTimestamp" ])]
    public DateTime $createdDate;

    #[DatabaseColumn("last_used_date", [ DateTimeParser::class, "setTimestampNullable" ])]
    public DateTime|null $lastUsedDate;

    #[DatabaseColumn("plugin_id")]
    public int|null $pluginId;

    #[DatabaseColumn("deleted_at", [ DateTimeParser::class, "setTimestampNullable" ])]
    public DateTime|null $deletedAt;

}
