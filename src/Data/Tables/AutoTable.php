<?php

namespace SpaethTech\UCRM\SDK\Data\Tables;

use Collectible;
use Collection;
use CollectionClass;
use Data\Attributes\DatabaseColumn;
use Data\Attributes\DatabaseTable;
use Data\Maps\ColumnMap;
use Data\Maps\TableMap;
use ReflectionClass;
use ReflectionException;
use ReflectionProperty;
use SpaethTech\UCRM\SDK\PluginLog;
use UcrmDatabase;

/**
 * @template T of AutoTable
 */
abstract class AutoTable extends Collectible
{
    private const PROPERTY_FILTER = ReflectionProperty::IS_PUBLIC | ReflectionProperty::IS_PROTECTED;

    /** @var TableMap[] $models */
    protected static array $models = [];


    public function __construct(array $properties = [])
    {
        $child = get_called_class();
        $table = self::reflectModel($child);

        foreach($properties as $columnName => $columnValue)
        {
            if (array_key_exists($columnName, $table->columns))
            {
                $column = $table->columns[$columnName];

                $this->{$column->propertyName} = match(count($column->setter))
                {
                    1 => call_user_func_array([ $this, $column->setter[0] ], [ $columnValue ]),
                    2 => call_user_func_array([ $column->setter[0], $column->setter[1] ], [ $columnValue ]),
                    default => $columnValue
                };
            }

            // Skip, not mapped?
        }

    }

    public static function all(): array|false
    {
        $child = get_called_class();
        $table = self::reflectModel($child);



        return array_map(fn($row) => new $child($row), UcrmDatabase::select($table->name));
    }

    /**
     * @param string $expression
     *
     * @return Collection<T>
     */
    public static function where(string $expression): Collection //array
    {
        $child = get_called_class();
        $table = self::reflectModel($child);

        $sql = <<< EOF
            SELECT *
            FROM $table->name
            WHERE $expression;
            EOF;

        $results = array_map(fn($row) => new $child($row), UcrmDatabase::query($sql));

        var_dump($results);
        var_dump($table->collectionClass);

        $collection = new $table->collectionClass($child, $results);
        var_dump($collection);

        return $collection;
    }


    /** @noinspection PhpSameParameterValueInspection */
    private static function reflectModel(string $child, bool $allowCached = true): TableMap
    {
        if (array_key_exists($child, self::$models) && $allowCached)
            return self::$models[$child];

        try
        {
            $reflected = new ReflectionClass($child);
        }
        catch (ReflectionException $e)
        {
            return PluginLog::error("An error occurred while reflecting on the Model!",
                [ "class" => $child, "message" => $e ]);
        }

        $mapName = "";
        $mapCollectionClass = "";
        $mapColumns = [];

        foreach($reflected->getAttributes(DatabaseTable::class) as $attribute)
        {
            /** @var DatabaseTable $table */
            $table = $attribute->newInstance();
            $mapName = $table->name;
        }

        foreach($reflected->getAttributes(CollectionClass::class) as $attribute)
        {
            /** @var CollectionClass $instance */
            $instance = $attribute->newInstance();
            $mapCollectionClass = $instance->class;
        }

        foreach($reflected->getProperties(self::PROPERTY_FILTER) as $property)
        {
            $columnMap = self::getColumnInfo($property);
            $mapColumns[$columnMap->columnName] = $columnMap;
        }

        // FUTURE: Other attributes???

        if (count($mapColumns) === 0)
        {
            PluginLog::warning("No public/protected properties were parsed for class '$mapName'");
        }

        return self::$models[$child] = new TableMap($mapName, $mapCollectionClass, $mapColumns);
    }


    private static function getColumnInfo(ReflectionProperty $property): ColumnMap
    {
        $map = new ColumnMap(null, $property->name, []);

        foreach($property->getAttributes(DatabaseColumn::class) as $attribute)
        {
            /** @var DatabaseColumn $column */
            $column = $attribute->newInstance();
            $map->columnName = $column->name;
            $map->setter = $column->setter;
        }

        $map->columnName ??= $property->name;

        return $map;
    }


}

