<?php
declare(strict_types=1);

namespace SpaethTech\UCRM\SDK\Data\Attributes;

use Attribute;

#[Attribute(Attribute::TARGET_CLASS)]
class DatabaseTable
{
    public function __construct(public string $name)
    {
    }
}
