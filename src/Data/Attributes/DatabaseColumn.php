<?php
declare(strict_types=1);

namespace SpaethTech\UCRM\SDK\Data\Attributes;

use Attribute;
use SpaethTech\UCRM\SDK\Data\Parsers\ColumnParserInterface;

#[Attribute(Attribute::TARGET_PROPERTY)]
class DatabaseColumn
{

    public function __construct(public string $name, public array $setter = [], public array $getter = [])
    {
    }
}
