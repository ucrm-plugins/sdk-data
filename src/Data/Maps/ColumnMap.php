<?php

namespace SpaethTech\UCRM\SDK\Data\Maps;


class ColumnMap
{

    /**
     * @param string|null $columnName
     * @param string|null $propertyName
     * @param array $setter
     * @param array $getter
     */
    public function __construct(
        public string|null $columnName = null,
        public string|null $propertyName = null,
        public array $setter = [],
        public array $getter = []
    )
    {

    }


}
