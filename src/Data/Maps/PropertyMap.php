<?php

namespace SpaethTech\UCRM\SDK\Data\Maps;

class PropertyMap
{
    public function __construct(public string $name, public mixed $value)
    {
    }
}
