<?php

namespace SpaethTech\UCRM\SDK\Data\Maps;

use Collection;

class TableMap
{
    /**
     * @param string $name
     * @param class-string<Collection> $collectionClass
     * @param ColumnMap[] $columns
     */
    public function __construct(
        public string $name,
        public string $collectionClass,
        public array $columns = []
    )
    {
    }


}
