<?php

namespace SpaethTech\UCRM\SDK\Data\Collections;

use Collection;
use Data\Tables\PluginTable;

/**
 * @extends Collection<PluginTable>
 */
class PluginCollection extends Collection
{
}
