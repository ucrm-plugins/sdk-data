<?php

namespace SpaethTech\UCRM\SDK\Data\Collections;

use Collection;
use Data\Tables\OptionTable;

/**
 * @extends Collection<OptionTable>
 */
class OptionCollection extends Collection
{
}
