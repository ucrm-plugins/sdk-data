<?php

namespace SpaethTech\UCRM\SDK\Data\Collections;

use Collection;
use Data\Tables\AppKeyTable;

/**
 * @extends Collection<AppKeyTable>
 */
class AppKeyCollection extends Collection
{
}
